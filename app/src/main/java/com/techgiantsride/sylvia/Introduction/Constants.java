package com.techgiantsride.sylvia.Introduction;

public class Constants {


    private Constants() {
    }

    public static final String mainUrl = "https://retan.website/km82pn6l";
    public static final String moderationUrl = "https://retan.website/km82pn6f";
    public static final String appsFlyerKey = "AcYFTpvc9wT7EErCK548rg";
    public static final String oneSignalAppId = "c6fc36a3-9155-4c86-b3a0-263de0508025";
    public static final String appName = "com.techgiantsride.sylvia";
    public static final Boolean debugMode = false;
}

